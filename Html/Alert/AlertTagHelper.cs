﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TagHelpers.Html.Alert
{
    [HtmlTargetElement(Attributes = "alert-*")]
    public class AlertTagHelper : AppTagHelper
    {
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            string str1 = "alert";
            string cssClass = str1 + "-";
            string str2 = "alert-custom ";
            TagHelperAttribute tagHelperAttribute1 =
                ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                    (Func<TagHelperAttribute, bool>) (a => a.Name.StartsWith(cssClass)));
            if (tagHelperAttribute1 == null)
                return;
            string str3 = str2 + this.AddCss(output, "notice", "alert-");
            string str4 = tagHelperAttribute1.Name.Replace(cssClass, "");
            if (output.HasAttribute("outline"))
                cssClass += "outline-";
            output.Attributes.SetAttribute("class", (object) (str1 + " " + cssClass + str4 + " " + str3));
            output.Attributes.RemoveAll(cssClass + str4);
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            if (output.HasAttribute("icon"))
            {
                TagHelperAttribute tagHelperAttribute2 =
                    ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                        (Func<TagHelperAttribute, bool>) (x => x.Name == "icon"));
                TagBuilder tagBuilder = new TagBuilder("i");
                tagBuilder.AddCssClass(tagHelperAttribute2?.Value?.ToString());
                TagBuilder tag = new TagBuilder("div");
                tag.AddCssClass("alert-icon");
                tag.AppendHtml((IHtmlContent) tagBuilder);
                output.Content.AppendHtml((IHtmlContent) tag);
            }

            TagBuilder tag1 = new TagBuilder("div");
            tag1.AddCssClass("alert-text");
            tag1.AppendHtml((IHtmlContent) childContentAsync);
            output.Content.AppendHtml((IHtmlContent) tag1);
            if (!output.HasAttribute("alert-close"))
                return;
            TagBuilder tagBuilder1 = new TagBuilder("i");
            tagBuilder1.AddCssClass("ki ki-close");
            TagBuilder tag2 = new TagBuilder("span");
            tag2.SetAttribute("aria-hidden", "true");
            tag2.AppendHtml((IHtmlContent) tagBuilder1);
            TagBuilder tag3 = new TagBuilder("button");
            tag3.SetAttribute("type", "button");
            tag3.SetAttribute("data-dismiss", "alert");
            tag3.SetAttribute("aria-label", "Kapat");
            tag3.AddCssClass("close");
            tag3.AppendHtml((IHtmlContent) tag2);
            TagBuilder tag4 = new TagBuilder("div");
            tag4.AddCssClass("alert-close");
            tag4.AppendHtml((IHtmlContent) tag3);
            output.Content.AppendHtml((IHtmlContent) tag4);
        }

        public string AddCss(TagHelperOutput output, string css, string prefix = "")
        {
            return ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                (Func<TagHelperAttribute, bool>) (x => x.Name == css)) != null
                ? " " + prefix + css + " "
                : "";
        }
    }
}
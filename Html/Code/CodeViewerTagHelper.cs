﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TagHelpers.Html.Code
{
    [HtmlTargetElement("code-card")]
    public class CodeViewerTagHelper : TagHelper
    {
        [HtmlAttributeName("title")] public string Title { get; set; }

        private async Task<TagHelperOutput> CreateCodeElement(TagHelperContext context, string childContent)
        {
            CodeTagHelper codeTagHelper = new CodeTagHelper()
            {
                Content = childContent
            };
            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("div");
            await ((TagHelper) codeTagHelper).ProcessAsync(context, tagHelperOutput);
            TagHelperOutput tagHelperOutput1 = tagHelperOutput;
            tagHelperOutput = null;
            return tagHelperOutput1;
        }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            TagBuilder tagBuilder = new TagBuilder("div");
            tagBuilder.AddCssClass("example-tools justify-content-center");
            TagBuilder tagBuilder1 = new TagBuilder("div");
            tagBuilder1.AddCssClass("card-toolbar");
            tagBuilder1.InnerHtml.AppendHtml(tagBuilder);
            TagBuilder tagBuilder2 = new TagBuilder("div");
            tagBuilder2.AddCssClass("card-header");
            if (!string.IsNullOrEmpty(this.Title))
            {
                TagBuilder tagBuilder3 = new TagBuilder("div");
                tagBuilder3.AddCssClass("card-title");
                tagBuilder3.InnerHtml.AppendHtml(this.Title);
                tagBuilder2.InnerHtml.AppendHtml(tagBuilder3);
            }

            tagBuilder2.InnerHtml.AppendHtml(tagBuilder1);
            TagBuilder tagBuilder4 = new TagBuilder("div");
            tagBuilder4.AddCssClass("card-body");
            tagBuilder4.InnerHtml.AppendHtml(childContentAsync);
            tagBuilder4.InnerHtml.AppendHtml(this.CreateCodeElement(context, childContentAsync.GetContent()).Result
                .Content.GetContent());
            output.TagName = ("div");
            output.Attributes.SetAttribute("class", "col-lg-12");
            TagBuilder tagBuilder5 = new TagBuilder("div");
            tagBuilder5.AddCssClass("card card-custom gutter-b example example-compact");
            tagBuilder5.InnerHtml.AppendHtml(tagBuilder2);
            tagBuilder5.InnerHtml.AppendHtml(tagBuilder4);
            output.Content.AppendHtml(tagBuilder5);
        }
    }
}
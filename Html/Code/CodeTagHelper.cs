﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TagHelpers.Html.Code
{
    [HtmlTargetElement("code-view")]
    public class CodeTagHelper : TagHelper
    {
        [HtmlAttributeName("height")] public string Height { get; set; }

        public string Content { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            context.Items.TryAdd<object, object>((object) "code-view", (object) "haro");
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            TagBuilder element1 = new TagBuilder("code");
            element1.AddCssClass("language-html");
            if (string.IsNullOrEmpty(this.Content))
                element1.InnerHtml.Append(childContentAsync.GetContent());
            else
                element1.InnerHtml.Append(this.Content);
            TagBuilder element2 = element1.WrapElement("pre", "");
            element2.Attributes.Add("style", "height: " + this.Height + ";");
            TagBuilder tagBuilder1 = element2.WrapElement("div", "example-highlight");
            TagBuilder tagBuilder2 = new TagBuilder("span");
            tagBuilder2.AddCssClass("example-copy");
            tagBuilder2.Attributes.Add("data-toggle", "tooltip");
            tagBuilder2.Attributes.Add("title", "Kopyala");
            output.TagName = ("div");
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.Add("class", (object) "example-code");
            output.Content.AppendHtml((IHtmlContent) tagBuilder2);
            output.Content.AppendHtml((IHtmlContent) tagBuilder1);
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TagHelpers.Html.Progress
{
    [HtmlTargetElement(Attributes = "progress")]
    public class ProgressTagHelper : AppTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperAttribute tagHelperAttribute =
                ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                    (Func<TagHelperAttribute, bool>) (x => x.Name == "progress"));
            if (tagHelperAttribute == null)
                return;
            string str = tagHelperAttribute.Value.ToString();
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("progress-bar");
            tag.SetAttribute("role", "progressbar");
            tag.SetAttribute("style", "width: " + str + "%;");
            tag.SetAttribute("aria-valuenow", str);
            tag.SetAttribute("aria-valuemin", "0");
            tag.SetAttribute(" aria-valuemax", "100");
            tag.InnerHtml.AppendHtml(str + "%");
            output.TagName = ("div");
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("class", (object) "progress");
            output.Content.AppendHtml((IHtmlContent) tag);
        }
    }
}
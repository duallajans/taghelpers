﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Anchor
{
    [HtmlTargetElement("a", Attributes = "asp-with-query")]
    public class WithQueryTagHelper : AnchorTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SetAttribute("asp-route-haro", "maro");
        }

        public WithQueryTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }
    }
}
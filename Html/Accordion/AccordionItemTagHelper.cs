﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Utilities.String;

namespace TagHelpers.Html.Accordion
{
    [HtmlTargetElement("accordion-item")]
    public class AccordionItemTagHelper : TagHelper
    {
        [HtmlAttributeName("id")] public string Id { get; set; }

        [HtmlAttributeName("title")] public string Title { get; set; }

        [HtmlAttributeName("show")] public bool Show { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = UniqueIdentifier.GenerateStr();
            }

            TagBuilder tagBuilder = new TagBuilder("div");
            tagBuilder.AddCssClass("card-title");
            if (!Show)
            {
                tagBuilder.AddCssClass("collapsed");
            }

            tagBuilder.SetAttribute("data-toggle", "collapse");
            tagBuilder.SetAttribute("data-target", string.Concat("#", this.Id));
            tagBuilder.SetAttribute("aria-expanded", "false");
            tagBuilder.AppendHtml(this.Title);
            TagBuilder tagBuilder1 = new TagBuilder("div");
            tagBuilder1.AddCssClass("card-header");
            tagBuilder1.AppendHtml(tagBuilder);
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            TagBuilder tagBuilder2 = new TagBuilder("div");
            tagBuilder2.AddCssClass("card-body");
            tagBuilder2.AppendHtml(childContentAsync);
            TagBuilder tagBuilder3 = new TagBuilder("div");
            tagBuilder3.AddCssClass("collapse");
            if (Show)
            {
                tagBuilder3.AddCssClass("show");
            }

            tagBuilder3.SetAttribute("id", this.Id);
            string str = context.Items[typeof(AccordionTagHelper)].ToString();
            tagBuilder3.SetAttribute("data-parent", string.Concat("#", str));
            tagBuilder3.AppendHtml(tagBuilder2);
            output.TagName = ("div");
            output.SetAttribute("class", "card");
            output.Content.AppendHtml(tagBuilder1);
            output.Content.AppendHtml(tagBuilder3);
        }
    }
}
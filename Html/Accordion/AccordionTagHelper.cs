﻿using Core.Utilities.String;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Accordion
{
    [HtmlTargetElement("accordion")]
    public class AccordionTagHelper : TagHelper
    {
        [HtmlAttributeName("id")] public string Id { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(Id)) Id = UniqueIdentifier.Generate();
            context.Items.Add(typeof(AccordionTagHelper), Id);
            output.TagName = "div";
            output.SetAttribute("class", "accordion accordion-toggle-arrow");
            output.SetAttribute("id", this.Id);
        }
    }
}
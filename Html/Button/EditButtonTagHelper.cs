﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Button
{
    [HtmlTargetElement(Attributes = "edit-button")]
    public class EditButtonTagHelper : AppTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SetAttribute("class", "btn btn-sm btn-clean btn-icon mr-2");
            output.Content.AppendHtml("<i class='ki ki-gear'></i>");
        }
    }
}
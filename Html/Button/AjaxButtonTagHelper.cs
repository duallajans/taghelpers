﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Button
{
    [HtmlTargetElement(Attributes = "ajax-url")]
    public class AjaxButtonTagHelper : AppTagHelper
    {
        [HtmlAttributeName("ajax-url")] public string Url { get; set; }

        [HtmlAttributeName("target")] public string Target { get; set; }

        [HtmlAttributeName("method")] public string Method { get; set; } = "GET";

        [HtmlAttributeName("begin")] public string Begin { get; set; } = "abBegin";

        [HtmlAttributeName("failure")] public string Fail { get; set; } = "abFail";

        [HtmlAttributeName("success")] public string Success { get; set; } = "abSuccess";

        [HtmlAttributeName("complete")] public string Complete { get; set; } = "abComplete";

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            output.Attributes.Add("data-ajax", "true");
            output.Attributes.Add("data-ajax-url", Url);
            output.Attributes.Add("data-ajax-method", Method);
            output.Attributes.Add("data-ajax-update", Target);
            output.Attributes.Add("data-ajax-begin", Begin);
            output.Attributes.Add("data-ajax-failure", Fail);
            output.Attributes.Add("data-ajax-success", Success);
            output.Attributes.Add("data-ajax-complete", Complete);
        }
    }
}
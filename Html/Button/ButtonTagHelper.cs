﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TagHelpers.Html.Button
{
    [HtmlTargetElement(Attributes = "btn-*")]
    public class PrimaryButtonTagHelper : AppTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            string str1 = "btn-";
            string str2 = "";
            TagHelperAttribute tagHelperAttribute =
                ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                    (Func<TagHelperAttribute, bool>) (a => a.Name.StartsWith("btn-")));
            if (tagHelperAttribute == null)
                return;
            if (output.Attributes.FirstOrDefault(x => x.Name == "light") != null)
            {
                str1 += "light-";
                str2 = " font-weight-bolder ";
                output.Attributes.RemoveAll("light");
            }

            if (((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                (Func<TagHelperAttribute, bool>) (x => x.Name == "outline")) != null)
            {
                str1 += "outline-";
                str2 = " font-weight-bold ";
                output.Attributes.RemoveAll("outline");
            }

            string str3 = str2 + this.AddCss(output, "active") + this.AddBtnCss(output, "sm") +
                          this.AddBtnCss(output, "lg") + this.AddBtnCss(output, "block") +
                          this.AddBtnCss(output, "shadow") + this.AddBtnCss(output, "icon");
            string str4 = tagHelperAttribute.Name.Replace("btn-", "");
            string str5 = "";
            if (output.HasAttribute("class"))
            {
                str5 = ((ReadOnlyTagHelperAttributeList) output.Attributes).FirstOrDefault(x => x.Name == "class").Value
                    .ToString();
                output.Attributes.RemoveAll("class");
            }

            output.Attributes.SetAttribute("class", (object) ("btn " + str1 + str4 + " " + str3 + " " + str5));
            output.Attributes.RemoveAll("btn-" + str4);
        }

        public string AddCss(TagHelperOutput output, string css)
        {
            return ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                (Func<TagHelperAttribute, bool>) (x => x.Name == css)) != null
                ? " " + css + " "
                : "";
        }

        public string AddBtnCss(TagHelperOutput output, string css)
        {
            return ((IEnumerable<TagHelperAttribute>) output.Attributes).FirstOrDefault<TagHelperAttribute>(
                (Func<TagHelperAttribute, bool>) (x => x.Name == css)) != null
                ? " btn-" + css + " "
                : "";
        }
    }
}
﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Button
{
    [HtmlTargetElement(Attributes = "delete-button")]
    public class DeleteButtonTagHelper : AppTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SetAttribute("class", "btn btn-sm btn-clean btn-icon mr-2 confirmLink");
            output.Content.AppendHtml("<i class='ki ki-close'></i>");
            base.Process(context, output);
        }
    }
}
﻿using Core.Utilities.String;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Image
{
    [HtmlTargetElement("overlay-image")]
    public class OverlayImageTagHelper : AppTagHelper
    {
        [HtmlAttributeName("id")] public string Id { get; set; }

        [HtmlAttributeName("src")] public string Image { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            TagBuilder tag1 = new TagBuilder("img");
            tag1.TagRenderMode = TagRenderMode.SelfClosing;
            tag1.SetAttribute("src", this.Image);
            tag1.SetAttribute("style", "object-fit: contain;max-height: 250px;");
            tag1.AddCssClass("w-100 rounded");
            TagBuilder tag2 = new TagBuilder("div");
            tag2.AddCssClass("overlay-wrapper");
            tag2.AppendHtml((IHtmlContent) tag1);
            TagBuilder tag3 = new TagBuilder("div");
            tag3.AddCssClass("overlay-layer align-items-center justify-content-center");
            TagHelperContent result = output.GetChildContentAsync().Result;
            tag3.AppendHtml((IHtmlContent) result);
            TagBuilder tag4 = new TagBuilder("div");
            tag4.AddCssClass("overlay w-250px h-250px d-flex align-items-center");
            tag4.AppendHtml((IHtmlContent) tag2);
            tag4.AppendHtml((IHtmlContent) tag3);
            output.TagName = ("div");
            if (string.IsNullOrEmpty(this.Id))
                this.Id = UniqueIdentifier.Generate();
            output.TagMode = TagMode.StartTagAndEndTag;
            output.SetAttribute("class", "col-lg-3");
            output.SetAttribute("id", this.Id);
            output.Content.AppendHtml((IHtmlContent) tag4);
        }
    }
}
﻿using Microsoft.AspNetCore.Html;

namespace TagHelpers.Html.Card
{
    public class CardContext
    {
        public IHtmlContent Header { get; set; }

        public IHtmlContent Body { get; set; }

        public IHtmlContent Footer { get; set; }
    }
}
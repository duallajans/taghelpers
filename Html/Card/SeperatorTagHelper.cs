﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Card
{
    [HtmlTargetElement(Attributes = "seperator")]
    public class SeperatorTagHelper : AppTagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = ("div");
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Attributes.SetAttribute("class", (object) "separator separator-dashed my-10");
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Html.Card
{
    [HtmlTargetElement("card-header", ParentTag = "card")]
    public class CardHeaderTagHelper : TagHelper
    {
        [HtmlAttributeName("title")] public string Title { get; set; }
        [HtmlAttributeName("sub-title")] public string SubTitle { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            CardContext cardContext = (CardContext) context.Items[(object) typeof(CardTagHelper)];
            TagBuilder tagBuilder1 = new TagBuilder("div");
            tagBuilder1.AddCssClass("example-tools justify-content-center");
            tagBuilder1.InnerHtml.AppendHtml(childContentAsync.GetContent());
            TagBuilder tagBuilder2 = new TagBuilder("div");
            tagBuilder2.AddCssClass("card-toolbar");
            tagBuilder2.InnerHtml.AppendHtml((IHtmlContent) tagBuilder1);
            TagBuilder tagBuilder3 = new TagBuilder("div");
            tagBuilder3.AddCssClass("card-header");
            if (!string.IsNullOrEmpty(this.Title))
            {
                TagBuilder tagBuilder4 = new TagBuilder("div");
                tagBuilder4.AddCssClass("card-title");
                tagBuilder4.InnerHtml.AppendHtml(this.Title);
                if (!string.IsNullOrEmpty(SubTitle))
                {
                    TagBuilder subTitelTag = new TagBuilder("small");
                    tagBuilder4.AddCssClass("ml-3");
                    subTitelTag.InnerHtml.AppendHtml(SubTitle);
                    tagBuilder4.InnerHtml.AppendHtml(subTitelTag);
                }
                tagBuilder3.InnerHtml.AppendHtml(tagBuilder4);
            }

            tagBuilder3.InnerHtml.AppendHtml((IHtmlContent) tagBuilder2);
            TagHelperContent tagHelperContent = childContentAsync.SetHtmlContent((IHtmlContent) tagBuilder3);
            cardContext.Header = (IHtmlContent) tagHelperContent;
            output.SuppressOutput();
        }
    }
}
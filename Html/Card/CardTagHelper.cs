﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TagHelpers.Html.Card
{
    [RestrictChildren("card-body", new string[] {"card-footer", "card-header"})]
    public class CardTagHelper : TagHelper
    {
        [HtmlAttributeName("col")] public string Column { get; set; } = "col-lg-12";
        [HtmlAttributeName("class")] public string Class { get; set; }
        [HtmlAttributeName("id")] public string Id { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            CardContext cardContext = new CardContext();
            if (!context.Items.ContainsKey(typeof(CardTagHelper)))
            {
                context.Items.Add(typeof(CardTagHelper), cardContext);
            }

            await output.GetChildContentAsync();
            output.TagName = ("div");
            output.Attributes.SetAttribute("class", Column);
            output.SetAttribute("id", this.Id);
            TagBuilder tagBuilder = new TagBuilder("div");
            tagBuilder.AddCssClass(string.Concat("card card-custom my-5 ", this.Class));
            if (cardContext.Header != null)
            {
                tagBuilder.InnerHtml.AppendHtml(cardContext.Header);
            }

            if (cardContext.Body != null)
            {
                tagBuilder.InnerHtml.AppendHtml(cardContext.Body);
            }

            if (cardContext.Footer != null)
            {
                tagBuilder.InnerHtml.AppendHtml(cardContext.Footer);
            }

            output.Content.AppendHtml(tagBuilder);
        }
    }
}
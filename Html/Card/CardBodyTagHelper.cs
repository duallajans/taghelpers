﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Html.Card
{
    [HtmlTargetElement("card-body", ParentTag = "card")]
    public class CardBodyTagHelper : TagHelper
    {
        [HtmlAttributeName("id")] public string Id { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            CardContext cardContext = (CardContext) context.Items[typeof(CardTagHelper)];
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("card-body");
            tag.SetAttribute("id", this.Id);
            tag.InnerHtml.AppendHtml(childContentAsync.GetContent());
            TagHelperContent tagHelperContent = childContentAsync.SetHtmlContent((IHtmlContent) tag);
            cardContext.Body = (IHtmlContent) tagHelperContent;
            output.SuppressOutput();
        }
    }
}
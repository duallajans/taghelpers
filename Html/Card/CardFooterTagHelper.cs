﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Html.Card
{
    [HtmlTargetElement("card-footer", ParentTag = "card")]
    public class CardFooterTagHelper : TagHelper
    {
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            CardContext cardContext = (CardContext) context.Items[(object) typeof(CardTagHelper)];
            TagBuilder tagBuilder = new TagBuilder("div");
            tagBuilder.AddCssClass("card-footer");
            tagBuilder.InnerHtml.AppendHtml(childContentAsync.GetContent());
            TagHelperContent tagHelperContent = childContentAsync.SetHtmlContent((IHtmlContent) tagBuilder);
            cardContext.Footer = (IHtmlContent) tagHelperContent;
            output.SuppressOutput();
        }
    }
}
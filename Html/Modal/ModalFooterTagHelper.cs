﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Html.Modal
{
    [HtmlTargetElement("modal-footer", ParentTag = "modal")]
    public class ModalFooterTagHelper : AppTagHelper
    {
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            DefaultTagHelperContent tagHelperContent = new DefaultTagHelperContent();
            ((TagHelperContent) tagHelperContent).AppendHtml((IHtmlContent) childContentAsync);
            ModalContext modalContext = (ModalContext) context.Items[(object) typeof(ModalTagHelper)];
            modalContext.Footer = (IHtmlContent) tagHelperContent;
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("modal-footer");
            tag.AppendHtml(
                "<button type=\"button\" class=\"btn btn-light-primary font-weight-bold\" data-dismiss=\"modal\">Kapat</button>");
            tag.AppendHtml(childContentAsync.GetContent());
            modalContext.Footer = (IHtmlContent) tag;
            output.SuppressOutput();
        }
    }
}
﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;

namespace TagHelpers.Html.Modal
{
    [HtmlTargetElement(Attributes = "modal")]
    public class ModalToggleHelper : TagHelper
    {
        [HtmlAttributeName("modal")] public string Modal { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(Modal))
                throw new Exception("Modal is required");
            output.Attributes.Add("type", "button");
            output.Attributes.SetAttribute("data-toggle", "modal");
            output.Attributes.SetAttribute("data-target", "#" + Modal);
        }
    }
}
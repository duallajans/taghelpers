﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TagHelpers.Html.Modal
{
    [RestrictChildren("modal-body", new string[] {"modal-footer"})]
    [HtmlTargetElement("modal")]
    public class ModalTagHelper : TagHelper
    {
        [HtmlAttributeName("id")] public string Id { get; set; }
        [HtmlAttributeName("title")] public string Title { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(this.Id))
            {
                throw new Exception("Id is required");
            }

            ModalContext modalContext = new ModalContext();
            context.Items.Add(typeof(ModalTagHelper), modalContext);
            await output.GetChildContentAsync();
            TagBuilder tagBuilder = new TagBuilder("h5");
            tagBuilder.AddCssClass("modal-title");
            tagBuilder.SetAttribute("Id", string.Concat(this.Id, "Label"));
            tagBuilder.InnerHtml.AppendHtml(this.Title);
            TagBuilder tagBuilder1 = new TagBuilder("button");
            tagBuilder1.SetAttribute("type", "button");
            tagBuilder1.SetAttribute("class", "close");
            tagBuilder1.SetAttribute("data-dismiss", "modal");
            tagBuilder1.SetAttribute("aria-label", "Kapat");
            tagBuilder1.AppendHtml("<i aria-hidden='true' class='ki ki-close'></i>");
            TagBuilder tagBuilder2 = new TagBuilder("div");
            tagBuilder2.AddCssClass("modal-header");
            tagBuilder2.AppendHtml(tagBuilder);
            tagBuilder2.AppendHtml(tagBuilder1);
            TagBuilder tagBuilder3 = new TagBuilder("div");
            tagBuilder3.AddCssClass("modal-content");
            tagBuilder3.AppendHtml(tagBuilder2);
            if (modalContext.Body != null)
            {
                tagBuilder3.AppendHtml(modalContext.Body);
            }

            if (modalContext.Footer != null)
            {
                tagBuilder3.AppendHtml(modalContext.Footer);
            }

            TagBuilder tagBuilder4 = new TagBuilder("div");
            tagBuilder4.AddCssClass("modal-dialog modal-dialog-centered modal-xl");
            tagBuilder4.SetAttribute("role", "document");
            tagBuilder4.AppendHtml(tagBuilder3);
            output.TagName = ("div");
            output.SetAttribute("id", this.Id);
            output.SetAttribute("class", "modal fade");
            output.SetAttribute("tabindex", "-1");
            output.SetAttribute("role", "dialog");
            output.SetAttribute("aria-labelledby", string.Concat(this.Id, "Label"));
            output.SetAttribute("aria-hidden", "true");
            output.Content.AppendHtml(tagBuilder4);
            modalContext = null;
        }
    }
}
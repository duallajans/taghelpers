﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Html.Modal
{
    [HtmlTargetElement("modal-body", ParentTag = "modal")]
    public class ModalBodyTagHelper : AppTagHelper
    {
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            ModalContext modalContext = (ModalContext) context.Items[(object) typeof(ModalTagHelper)];
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("modal-body ");
            tag.AppendHtml((IHtmlContent) childContentAsync);
            TagBuilder tagBuilder = tag;
            modalContext.Body = (IHtmlContent) tagBuilder;
            output.SuppressOutput();
        }
    }
}
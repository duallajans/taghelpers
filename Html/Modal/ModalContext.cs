﻿using Microsoft.AspNetCore.Html;

namespace TagHelpers.Html.Modal
{
    public class ModalContext
    {
        public IHtmlContent Body { get; set; }

        public IHtmlContent Footer { get; set; }
    }
}
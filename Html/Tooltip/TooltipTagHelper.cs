﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers.Html.Tooltip
{
    [HtmlTargetElement(Attributes = "tooltip")]
    public class TooltipTagHelper : AppTagHelper
    {
        [HtmlAttributeName("tooltip")] public string Tooltip { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add("data-toggle", (object) "tooltip");
            output.Attributes.Add("title", (object) this.Tooltip);
        }
    }
}
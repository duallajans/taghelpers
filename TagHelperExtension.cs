﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace TagHelpers
{
    public static class TagHelperExtension
    {
        public static void AppendHtml(this TagBuilder tag, IHtmlContent html)
        {
            tag.InnerHtml.AppendHtml(html);
        }

        public static void AppendHtml(this TagBuilder tag, string str)
        {
            tag.InnerHtml.AppendHtml(str);
        }

        public static TagHelperOutput CreateTagHelperOutput(string tagName)
        {
            return new TagHelperOutput(
                tagName: tagName,
                attributes: new TagHelperAttributeList(),
                getChildContentAsync: (s, t) =>
                {
                    return Task.Factory.StartNew<TagHelperContent>(
                        () => new DefaultTagHelperContent());
                }
            );
        }

        public static bool HasAttribute(this TagHelperOutput tagHelperOutput, string str)
        {
            return tagHelperOutput.Attributes.Any(x => x.Name == str);
        }

        public static bool HasAttribute(this TagHelperOutput tagHelperOutput, Func<TagHelperAttribute, bool> func)
        {
            return (object) Enumerable.FirstOrDefault<TagHelperAttribute>(tagHelperOutput.Attributes, func) !=
                   (object) null;
        }

        public static void SetAttribute(this TagBuilder tag, string key, string value)
        {
            tag.Attributes.Add(key, value);
        }

        public static void SetAttribute(this TagHelperOutput tag, string key, string value)
        {
            tag.Attributes.Add(key, value);
        }

        private static void SetScript(this TagHelperOutput inputElement, ViewContext viewContext, string scr)
        {
            if (string.IsNullOrEmpty(scr))
            {
                return;
            }

            dynamic viewBag = ((dynamic) viewContext.ViewBag).RenderPartial != (dynamic) null;
            if ((!viewBag ? viewBag : viewBag & ((dynamic) viewContext.ViewBag).RenderPartial))
            {
                TagBuilder tagBuilder = new TagBuilder("script");
                tagBuilder.AppendHtml(scr);
                inputElement.PostContent.AppendHtml(tagBuilder);
                return;
            }

            IHtmlContent htmlString = new HtmlString(scr);
            string str = "Scripts";
            List<IHtmlContent> item = viewContext.TempData[str] as List<IHtmlContent>;
            if (item != null)
            {
                item.Add(htmlString);
            }
            else
            {
                List<IHtmlContent> list = new List<IHtmlContent>();
                list.Add(htmlString);
                item = list;
            }

            viewContext.TempData[str] = item;
        }

        public static TagBuilder WrapElement(this TagBuilder element, string tagName, string classValue = "")
        {
            TagBuilder tagBuilder = new TagBuilder(tagName);
            tagBuilder.AddCssClass(classValue);
            tagBuilder.InnerHtml.AppendHtml(element);
            return tagBuilder;
        }

        public static IHtmlContent WrapElements(List<IHtmlContent> elements, string tagName, string classValue = "")
        {
            TagBuilder tagBuilder = new TagBuilder(tagName);
            tagBuilder.AddCssClass(classValue);
            foreach (IHtmlContent element in elements)
            {
                tagBuilder.InnerHtml.AppendHtml(element);
            }

            return tagBuilder;
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;

namespace TagHelpers.Layout
{
    [HtmlTargetElement("bottom-script")]
    public class BottomScriptTagHelper : TagHelper
    {
        [HtmlAttributeNotBound] [ViewContext] public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string index1 = "Scripts";
            ITempDataDictionary tempData = ViewContext.TempData;
            if (tempData[index1] is List<IHtmlContent> ihtmlContentList)
            {
                output.TagName = ("script");
                output.TagMode = TagMode.StartTagAndEndTag;
                using (List<IHtmlContent>.Enumerator enumerator = ihtmlContentList.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        IHtmlContent current = enumerator.Current;
                        output.Content.AppendHtml(current);
                    }
                }
            }

            string index2 = "KTScripts";
            if (!(tempData[index2] is List<IHtmlContent> ihtmlContentList2))
                return;
            TagBuilder tag = new TagBuilder("script");
            tag.TagRenderMode = (TagRenderMode) 0;
            using (List<IHtmlContent>.Enumerator enumerator = ihtmlContentList2.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    IHtmlContent current = enumerator.Current;
                    tag.AppendHtml(current);
                }
            }

            output.PostElement.AppendHtml(tag);
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Utilities.String;

namespace TagHelpers.Table
{
    [HtmlTargetElement("basic-table")]
    public class BasicTableTagHelper : AppTagHelper
    {
        [HtmlAttributeName("titles")] public string[] Titles { get; set; }
        [HtmlAttributeName("admin")] public bool AdminTable { get; set; } = true;
        [HtmlAttributeName("class")] public string Class { get; set; }
        [HtmlAttributeName("id")] public string Id { get; set; }
        [HtmlAttributeName("caption")] public string Caption { get; set; }
        [ViewContext] [HtmlAttributeNotBound] public ViewContext ViewContext { get; set; }
        public string Script { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(Class))
            {
                Class = $"table table-striped table-hover " + (AdminTable ? "adminTable" : "");
            }
            else if (AdminTable)
            {
                Class += " adminTable";
            }

            if (string.IsNullOrEmpty(this.Id)) this.Id = UniqueIdentifier.Generate();

            if (AdminTable)
            {
                this.Script = string.Concat("$('#", this.Id,
                    "').DataTable({\"ordering\":false,\"language\":{\"url\":\"//cdn.datatables.net/plug-ins/1.10.21/i18n/Turkish.json\"}});");
            }

            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            TagBuilder tagBuilder = new TagBuilder("tr");
            string[] titles = this.Titles;
            for (int i = 0; i < (int) titles.Length; i++)
            {
                string str = titles[i];
                TagBuilder tagBuilder1 = new TagBuilder("th");
                tagBuilder1.SetAttribute("scope", "col");
                tagBuilder1.AppendHtml(str);
                tagBuilder.AppendHtml(tagBuilder1);
            }

            TagBuilder tagBuilder2 = new TagBuilder("thead");
            tagBuilder2.AppendHtml(tagBuilder);
            TagBuilder tagBuilder3 = new TagBuilder("tbody");
            tagBuilder3.AppendHtml(childContentAsync);
            
            var table = new TagBuilder("table");
            table.TagRenderMode = TagRenderMode.Normal;
            table.Attributes.Add("class", this.Class);
            if (!string.IsNullOrEmpty(Caption))
            {
                TagBuilder caption = new TagBuilder("caption");
                caption.AppendHtml(Caption);
                table.InnerHtml.AppendHtml(caption);
            }
            table.SetAttribute("id", this.Id);
            table.InnerHtml.AppendHtml(tagBuilder2);
            table.InnerHtml.AppendHtml(tagBuilder3);

            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Content.AppendHtml(table);
            output.Attributes.SetAttribute("class", "table-responsive");
            SetScript(output);
        }

        private void SetScript(TagHelperOutput inputElement)
        {
            if (string.IsNullOrEmpty(this.Script))
            {
                return;
            }

            dynamic viewBag = ((dynamic) this.ViewContext.ViewBag).RenderPartial != (dynamic) null;
            if ((!viewBag ? viewBag : viewBag & ((dynamic) this.ViewContext.ViewBag).RenderPartial))
            {
                TagBuilder tagBuilder = new TagBuilder("script");
                tagBuilder.AppendHtml(this.Script);
                inputElement.PostContent.AppendHtml(tagBuilder);
                return;
            }

            IHtmlContent htmlString = new HtmlString(this.Script);
            string str = "Scripts";
            List<IHtmlContent> item = this.ViewContext.TempData[str] as List<IHtmlContent>;
            if (item != null)
            {
                item.Add(htmlString);
            }
            else
            {
                List<IHtmlContent> list = new List<IHtmlContent>();
                list.Add(htmlString);
                item = list;
            }

            this.ViewContext.TempData[str] = item;
        }
    }
}
namespace TagHelpers.Models
{
    public class ChartLineModel
    {
        public string Identifier { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
    }
}
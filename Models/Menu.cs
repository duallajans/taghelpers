﻿// Decompiled with JetBrains decompiler
// Type: TagHelpers.Models.Menu
// Assembly: TagHelpers, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4F814C32-95AC-4E62-892D-EBBAA38E1D9F
// Assembly location: C:\Users\harun\Downloads\aracTavsiye\aracTavsiye\TagHelpers.dll

using System.Collections.Generic;

namespace TagHelpers.Models
{
  public class Menu
  {
    public Menu()
    {
    }

    public Menu(string text)
    {
      this.Text = text;
      this.Url = "#";
    }

    public Menu(string text, string url)
    {
      this.Text = text;
      this.Url = url;
    }

    public string Text { get; set; }

    public string Url { get; set; }

    public string Icon { get; set; }

    public int Badge { get; set; }

    public List<Menu> Menus { get; set; } = new List<Menu>();

    public MenuType Type { get; set; }
  }
}

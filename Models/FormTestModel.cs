﻿// Decompiled with JetBrains decompiler
// Type: TagHelpers.Models.FormTestModel
// Assembly: TagHelpers, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4F814C32-95AC-4E62-892D-EBBAA38E1D9F
// Assembly location: C:\Users\harun\Downloads\aracTavsiye\aracTavsiye\TagHelpers.dll

using System;

namespace TagHelpers.Models
{
  public class FormTestModel
  {
    public string Title { get; set; }

    public string Description { get; set; }

    public string Age { get; set; }

    public string Email { get; set; }

    public string Phone { get; set; }

    public string Image { get; set; }

    public string Content { get; set; }

    public string Gender { get; set; }

    public string Color { get; set; }

    public string Id { get; set; }

    public string Password { get; set; }

    public string YesOrNo { get; set; }

    public string MetaDescription { get; set; }

    public string File { get; set; }

    public DateTime DateTime { get; set; }

    public DateTime Date { get; set; }

    public string Time { get; set; }
  }
}

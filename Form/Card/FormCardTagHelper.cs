﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Utilities.String;

namespace TagHelpers.Form.Card
{
    [HtmlTargetElement("form-card")]
    public class FormCardTagHelper : AppTagHelper
    {
        [HtmlAttributeName("area")] public string Area { get; set; }

        [HtmlAttributeName("controller")] public string Controller { get; set; }

        [HtmlAttributeName("action")] public string Action { get; set; }

        [HtmlAttributeName("id")] public string Id { get; set; }

        [HtmlAttributeName("title")] public string Title { get; set; }

        [HtmlAttributeName("col")] public string Column { get; set; } = "col-lg-12";

        [HtmlAttributeName("method")] public string Method { get; set; } = "POST";

        [HtmlAttributeName("begin")] public string Begin { get; set; } = "ajaxFormBegin";

        [HtmlAttributeName("failure")] public string Fail { get; set; } = "ajaxFormFail";

        [HtmlAttributeName("success")] public string Success { get; set; } = "ajaxFormSuccess";

        [HtmlAttributeName("complete")] public string Complete { get; set; } = "ajaxFormComplete";

        [HtmlAttributeName("save-text")] public string SaveText { get; set; } = "Kaydet";

        [HtmlAttributeName("back-link")] public string BackLink { get; set; }

        [HtmlAttributeName("back-text")] public string BackText { get; set; } = "Listeye Dön";

        [ViewContext] [HtmlAttributeNotBound] public ViewContext ViewContext { get; set; }

        public IHtmlGenerator Generator { get; }


        public string Script { get; set; }

        public FormCardTagHelper(IHtmlGenerator htmlGenerator)
        {
            this.Generator = htmlGenerator;
        }

        private async Task<TagHelperOutput> CreateForm(TagHelperContext context)
        {
            FormTagHelper formTagHelper = new FormTagHelper(this.Generator);
            formTagHelper.ViewContext = ViewContext;
            formTagHelper.Area = Area;
            formTagHelper.Controller = Controller;
            formTagHelper.Action = Action;
            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("form");
            await formTagHelper.ProcessAsync(context, tagHelperOutput);
            tagHelperOutput.Attributes.Add("data-ajax", "true");
            tagHelperOutput.Attributes.Add("data-ajax-method", this.Method);
            tagHelperOutput.Attributes.Add("data-ajax-begin", this.Begin);
            tagHelperOutput.Attributes.Add("data-ajax-failure", this.Fail);
            tagHelperOutput.Attributes.Add("data-ajax-success", this.Success);
            tagHelperOutput.Attributes.Add("data-ajax-complete", this.Complete);
            tagHelperOutput.Attributes.Add("enctype", "multipart/form-data");
            TagHelperOutput tagHelperOutput1 = tagHelperOutput;
            tagHelperOutput = null;
            return tagHelperOutput1;
        }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (string.IsNullOrEmpty(this.Id))
            {
                this.Id = UniqueIdentifier.Generate();
            }

            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            TagBuilder tagBuilder = new TagBuilder("h3");
            tagBuilder.AddCssClass("card-label");
            tagBuilder.InnerHtml.Append(this.Title);
            TagBuilder tagBuilder1 = new TagBuilder("div");
            tagBuilder1.AddCssClass("card-title");
            tagBuilder1.InnerHtml.AppendHtml(tagBuilder);
            TagBuilder tagBuilder2 = new TagBuilder("div");
            tagBuilder2.AddCssClass("card-toolbar");
            if (!string.IsNullOrEmpty(this.BackLink))
            {
                TagBuilder tagBuilder3 = new TagBuilder("a");
                tagBuilder3.AddCssClass("btn btn-light-primary font-weight-bold mr-2");
                tagBuilder3.SetAttribute("href", this.BackLink);
                tagBuilder3.InnerHtml.AppendHtml(string.Concat("<i class='ki ki-long-arrow-back icon-sm'></i> ",
                    this.BackText));
                tagBuilder2.AppendHtml(tagBuilder3);
            }

            TagBuilder tagBuilder4 = new TagBuilder("button");
            tagBuilder4.AddCssClass("btn btn-primary");
            tagBuilder4.SetAttribute("type", "submit");
            tagBuilder4.AppendHtml(this.SaveText);
            tagBuilder2.AppendHtml(tagBuilder4);
            TagBuilder tagBuilder5 = new TagBuilder("div");
            tagBuilder5.AddCssClass("card-header");
            tagBuilder5.InnerHtml.AppendHtml(tagBuilder1);
            tagBuilder5.InnerHtml.AppendHtml(tagBuilder2);
            TagBuilder tagBuilder6 = new TagBuilder("div");
            tagBuilder6.AddCssClass("card-body");
            tagBuilder6.AppendHtml(childContentAsync);
            TagBuilder tagBuilder7 = new TagBuilder("div");
            tagBuilder7.AddCssClass("card card-custom card-sticky my-5");
            tagBuilder7.SetAttribute("id", this.Id);
            tagBuilder7.AppendHtml(tagBuilder5);
            tagBuilder7.AppendHtml(tagBuilder6);
            TagHelperOutput tagHelperOutput = await this.CreateForm(context);
            tagHelperOutput.Content.AppendHtml(tagBuilder7);
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.SetAttribute("class", this.Column);
            output.Content.AppendHtml(tagHelperOutput);
            this.SetScript(output);
        }

        private void SetScript(TagHelperOutput inputElement)
        {
            dynamic viewBag = ((dynamic) this.ViewContext.ViewBag).RenderPartial != (dynamic) null;
            if ((!viewBag ? viewBag : viewBag & ((dynamic) this.ViewContext.ViewBag).RenderPartial))
            {
                TagBuilder tagBuilder = new TagBuilder("script");
                tagBuilder.AppendHtml(string.Concat("\r\nKTLayoutStickyCard.init('", this.Id,
                    "');\r\nKTLayoutStickyCard.update();"));
                inputElement.PostContent.AppendHtml(tagBuilder);
                return;
            }

            IHtmlContent htmlString = new HtmlString(string.Concat("KTUtil.ready(function() {KTLayoutStickyCard.init('",
                this.Id, "');});"));
            string str = "KTScripts";
            List<IHtmlContent> item = this.ViewContext.TempData[str] as List<IHtmlContent>;
            if (item != null)
            {
                item.Add(htmlString);
            }
            else
            {
                List<IHtmlContent> list = new List<IHtmlContent>();
                list.Add(htmlString);
                item = list;
            }

            this.ViewContext.TempData[str] = item;
        }
    }
}
﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Utilities.String;

namespace TagHelpers.Form.Input
{
    [HtmlTargetElement("form-input")]
    public class FormInputTagHelper : AppTagHelper
    {
        [HtmlAttributeName("for")] public ModelExpression For { get; set; }

        [HtmlAttributeName("display")] public string DisplayName { get; set; }

        [HtmlAttributeName("type")] public FormInputType InputType { get; set; }

        [HtmlAttributeName("id")] public string Id { get; set; }

        [HtmlAttributeName("info")] public string Information { get; set; }

        [HtmlAttributeName("min")] public int? Min { get; set; }

        [HtmlAttributeName("max")] public int? Max { get; set; }

        [HtmlAttributeName("multiple")] public bool Multiple { get; set; }

        [HtmlAttributeName("readonly")] public bool ReadOnly { get; set; }

        [HtmlAttributeName("disabled")] public bool Disabled { get; set; }

        [HtmlAttributeName("select-items")] public List<SelectListItem> SelectItems { get; set; }

        [HtmlAttributeName("datavalues")]
        public List<KeyValuePair<string, string>> DataValues { get; set; } = new List<KeyValuePair<string, string>>();

        [HtmlAttributeName("extra")] public string Extra { get; set; }

        [HtmlAttributeName("value")] public string Value { get; set; }

        [HtmlAttributeName("image")] public string Image { get; set; }

        [HtmlAttributeName("images")] public List<string> Images { get; set; }
        [HtmlAttributeName("label")] public string Label { get; set; }
        public string Script { get; set; }

        public IHtmlGenerator Generator { get; }

        public FormInputTagHelper(IHtmlGenerator generator) : base()
        {
            Generator = generator;
        }

        [ViewContext] [HtmlAttributeNotBound] public ViewContext ViewContext { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            string str;
            if (this.For == null)
            {
                throw new Exception("For not bound");
            }

            if (string.IsNullOrEmpty(this.Id)) Id = UniqueIdentifier.Generate();

            if (string.IsNullOrEmpty(this.DisplayName)) DisplayName = For.Name;

            if (InputType == FormInputType.MetaDescription) Max = 168;

            if (SelectItems != null)
            {
                object model = For.Model;
                if (model != null) str = model.ToString();
                else str = null;
                SelectItems.ForEach(x =>
                {
                    if (x.Value == this.Value) x.Selected = true;
                });
            }

            var list = new List<IHtmlContent>();
            list.Add(await this.CreateLabelElement(context));
            TagHelperOutput tagHelperOutput = this.BaseInput(await this.CreateBaseInput(context));
            this.CreateScripts();
            tagHelperOutput = this.WrapInputElement(tagHelperOutput);
            list.Add(tagHelperOutput);
            if (!string.IsNullOrEmpty(Image))
            {
                TagBuilder tagBuilder = new TagBuilder("a");
                tagBuilder.SetAttribute("href", this.Image);
                tagBuilder.SetAttribute("target", "_blank");
                tagBuilder.AppendHtml("Dosyayı Görmek İçin Tıklayın");
                list.Add(tagBuilder);
            }

            if (!string.IsNullOrEmpty(Information))
            {
                var info = new TagBuilder("span");
                info.TagRenderMode = TagRenderMode.Normal;
                info.SetAttribute("class", "form-text text-muted");
                info.InnerHtml.SetContent(Information);
                list.Add(info);
            }

            output = this.WrapResult(output, list);
            SetScript(output);
        }

        private TagHelperOutput BaseInput(TagHelperOutput output)
        {
            output.Attributes.SetAttribute("id", this.Id);
            if (this.Multiple)
            {
                output.Attributes.Add("multiple", "multiple");
            }

            if (this.Disabled)
            {
                output.Attributes.Add("disabled", "disabled");
            }

            if (!output.Attributes.ContainsName("class"))
            {
                FormInputType inputType = this.InputType;
                if (inputType == FormInputType.Image || inputType == FormInputType.File)
                {
                    output.SetAttribute("class", "custom-file-input");
                }
                else
                {
                    output.Attributes.SetAttribute("class", "form-control");
                }
            }

            if (InputType == FormInputType.Date || InputType == FormInputType.DateTime)
            {
                output.Attributes.SetAttribute("autocomplete", "off");
            }

            if (this.Max.HasValue)
            {
                output.Attributes.Add("maxlength", this.Max.Value);
            }

            if (InputType == FormInputType.Emails)
            {
                output.SetAttribute("data-role", "tagsinput");
            }

            return output;
        }

        private async Task<TagHelperOutput> CreateBaseInput(TagHelperContext context)
        {
            TagHelperOutput tagHelperOutput;

            if (InputType == FormInputType.TextArea || InputType == FormInputType.TextEditor)
            {
                tagHelperOutput = await this.CreateTextareaElement(context);
            }
            else if (InputType == FormInputType.Select)
            {
                tagHelperOutput = await this.CreateSelectElement(context);
            }
            else if (InputType == FormInputType.RadioButton)
            {
                tagHelperOutput = this.CreateRadioElement(context);
            }
            else
            {
                tagHelperOutput = await CreateInputElement(context);
            }

            if (!DataValues.Any()) return tagHelperOutput;

            foreach (var x in DataValues)
            {
                tagHelperOutput.SetAttribute("data-" + x.Key, x.Value);
            }

            return tagHelperOutput;
        }

        private async Task<TagHelperOutput> CreateInputElement(TagHelperContext context)
        {
            InputTagHelper inputTagHelper = new InputTagHelper(this.Generator);
            inputTagHelper.For = (this.For);
            inputTagHelper.ViewContext = (this.ViewContext);
            inputTagHelper.Value = (this.Value);
            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("input");
            tagHelperOutput.Attributes.SetAttribute("type", this.GetInputTypeName(this.InputType));
            await inputTagHelper.ProcessAsync(context, tagHelperOutput);
            TagHelperOutput tagHelperOutput1 = tagHelperOutput;
            tagHelperOutput = null;
            return tagHelperOutput1;
        }

        private async Task<TagHelperOutput> CreateLabelElement(TagHelperContext context)
        {
            TagHelperOutput tagHelperOutput;
            TagHelperOutput tagHelperOutput1;
            if (this.InputType != FormInputType.Hidden)
            {
                LabelTagHelper labelTagHelper = new LabelTagHelper(this.Generator);
                labelTagHelper.For = (this.For);
                labelTagHelper.ViewContext = (this.ViewContext);
                tagHelperOutput1 = TagHelperExtension.CreateTagHelperOutput("label");
                await labelTagHelper.ProcessAsync(context, tagHelperOutput1);
                tagHelperOutput1.Attributes.RemoveAll("for");
                FormInputType inputType = this.InputType;
                if (inputType == FormInputType.Checkbox || inputType == FormInputType.Switch)
                {
                    tagHelperOutput1.Attributes.SetAttribute("class", "col-3 col-form-label");
                }

                if (inputType == FormInputType.Select)
                {
                    tagHelperOutput1.Attributes.SetAttribute("class", "col-12");
                }

                tagHelperOutput = tagHelperOutput1;
            }
            else
            {
                tagHelperOutput = null;
            }

            if (!string.IsNullOrEmpty(Label))
            {
                tagHelperOutput?.Content.SetContent(Label);
            }

            return tagHelperOutput;
        }

        private TagHelperOutput CreateRadioElement(TagHelperContext context)
        {
            List<TagBuilder> list = new List<TagBuilder>();
            foreach (SelectListItem selectItem in this.SelectItems)
            {
                TagBuilder tagBuilder = new TagBuilder("label");
                tagBuilder.Attributes.Add("class", "radio radio-solid");
                TagBuilder tagBuilder1 = new TagBuilder("input");
                tagBuilder1.Attributes.Add("type", "radio");
                tagBuilder1.Attributes.Add("name", this.For.Name);
                tagBuilder1.Attributes.Add("value", selectItem.Value);
                if (selectItem.Selected)
                {
                    tagBuilder1.SetAttribute("checked", "checked");
                }

                tagBuilder1.TagRenderMode = TagRenderMode.SelfClosing;
                TagBuilder tagBuilder2 = new TagBuilder("span");
                tagBuilder.InnerHtml.AppendHtml(tagBuilder1);
                tagBuilder.InnerHtml.AppendHtml(selectItem.Text);
                tagBuilder.InnerHtml.AppendHtml(tagBuilder2);
                list.Add(tagBuilder);
            }

            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("div");
            tagHelperOutput.Attributes.SetAttribute("class", "radio-inline");
            list.ForEach(x => tagHelperOutput.Content.AppendHtml(x));
            return tagHelperOutput;
        }

        private void CreateScripts()
        {
            switch (this.InputType)
            {
                case FormInputType.Phone:
                {
                    this.Script = string.Concat("$(\"#", this.Id,
                        "\").inputmask(\"mask\", {\"mask\": \"(999) 999-9999\"});");
                    return;
                }
                case FormInputType.Image:
                case FormInputType.Color:
                case FormInputType.Hidden:
                case FormInputType.Password:
                case FormInputType.Checkbox:
                case FormInputType.File:
                case FormInputType.Null:
                {
                    return;
                }
                case FormInputType.TextEditor:
                {
                    this.Script = string.Concat("$('#", this.Id, "').summernote({height: 150});");
                    return;
                }
                case FormInputType.Select:
                {
                    this.Script = string.Concat("$('#", this.Id,
                        "').select2({placeholder:'Seçiniz',allowClear:true});");
                    return;
                }
                case FormInputType.SeoUrl:
                {
                    this.Script = string.Concat(new string[]
                    {
                        "$('[name=\"", this.Extra, "\"]').keyup(function () {{ $('#", this.Id,
                        "').val(generateSeoUrl($('[name=\"", this.Extra, "\"]').val())); }});$('#", this.Id,
                        "').blur(function() { { $(this).val(generateSeoUrl($(this).val())); } });"
                    });
                    return;
                }
                case FormInputType.MetaDescription:
                {
                    this.Script = string.Concat("$('#", this.Id,
                        "').maxlength({warningClass: 'label label-warning label-rounded label-inline',limitReachedClass: 'label label-success label-rounded label-inline'});");
                    return;
                }
                case FormInputType.DateTime:
                {
                    Script = string.Concat("$('#", Id,
                        "').datetimepicker({todayHighlight: true,autoclose: true,todayBtn: true,format: 'dd/mm/yyyy hh:ii'});");
                    return;
                }
                case FormInputType.Date:
                {
                    Script = string.Concat("$('#", Id,
                        "').datepicker({format: 'yyyy-mm-dd',todayBtn: 'linked',clearBtn: true,orientation: 'bottom left',todayHighlight: true,templates: arrows = {leftArrow: '<i class=\"la la-angle-left\"></i>',rightArrow: '<i class=\"la la-angle-right\" ></i>'}});");
                    return;
                }
                case FormInputType.Time:
                {
                    this.Script = string.Concat("$('#", this.Id,
                        "').timepicker({minuteStep: 1,defaultTime: '',showSeconds: false,showMeridian: false,snapToStep: true});");
                    return;
                }
                case FormInputType.Price:
                    //Script = $@"$('#{Id}').mask('#,##0.00', {{reverse: true}});";
                    return;
                default:
                {
                    return;
                }
            }
        }

        private async Task<TagHelperOutput> CreateSelectElement(TagHelperContext context)
        {
            if (this.SelectItems == null)
            {
                this.SelectItems = new List<SelectListItem>();
            }

            SelectTagHelper selectTagHelper = new SelectTagHelper(this.Generator);
            selectTagHelper.For = (this.For);
            selectTagHelper.Items = (this.SelectItems);
            selectTagHelper.ViewContext = (this.ViewContext);
            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("select");
            if (this.Multiple)
            {
                tagHelperOutput.Attributes.Add("multiple", "multiple");
            }

            tagHelperOutput.SetAttribute("class", "form-control select2");

            await selectTagHelper.ProcessAsync(context, tagHelperOutput);
            TagHelperOutput tagHelperOutput1 = tagHelperOutput;
            tagHelperOutput = null;
            return tagHelperOutput1;
        }

        private async Task<TagHelperOutput> CreateTextareaElement(TagHelperContext context)
        {
            TextAreaTagHelper textAreaTagHelper = new TextAreaTagHelper(Generator);
            textAreaTagHelper.For = (For);
            textAreaTagHelper.ViewContext = (ViewContext);
            TagHelperOutput tagHelperOutput = TagHelperExtension.CreateTagHelperOutput("textarea");
            await textAreaTagHelper.ProcessAsync(context, tagHelperOutput);
            TagHelperOutput tagHelperOutput1 = tagHelperOutput;
            return tagHelperOutput1;
        }

        private string GetInputTypeName(FormInputType inputType)
        {
            switch (inputType)
            {
                case FormInputType.Text:
                case FormInputType.SeoUrl:
                case FormInputType.MetaDescription:
                case FormInputType.DateTime:
                case FormInputType.Date:
                case FormInputType.Time:
                case FormInputType.Emails:
                {
                    return "text";
                }
                case FormInputType.TextArea:
                case FormInputType.TextEditor:
                case FormInputType.Select:
                case FormInputType.Null:
                case FormInputType.RadioButton:
                {
                    throw new Exception("Form tipi işlenmemiş");
                }
                case FormInputType.Price:
                case FormInputType.Number:
                {
                    return "number";
                }
                case FormInputType.Email:
                {
                    return "email";
                }
                case FormInputType.Phone:
                {
                    return "tel";
                }
                case FormInputType.Image:
                case FormInputType.File:
                {
                    return "file";
                }
                case FormInputType.Color:
                {
                    return "color";
                }
                case FormInputType.Hidden:
                {
                    return "hidden";
                }
                case FormInputType.Password:
                {
                    return "password";
                }
                case FormInputType.Checkbox:
                case FormInputType.Switch:
                {
                    return "checkbox";
                }
                default:
                {
                    throw new Exception("Form tipi işlenmemiş");
                }
            }
        }


        private void SetScript(TagHelperOutput inputElement)
        {
            if (string.IsNullOrEmpty(this.Script))
            {
                return;
            }

            dynamic viewBag = ((dynamic) this.ViewContext.ViewBag).RenderPartial != (dynamic) null;
            if ((!viewBag ? viewBag : viewBag & ((dynamic) this.ViewContext.ViewBag).RenderPartial))
            {
                TagBuilder tagBuilder = new TagBuilder("script");
                tagBuilder.AppendHtml(this.Script);
                inputElement.PostContent.AppendHtml(tagBuilder);
                return;
            }

            IHtmlContent htmlString = new HtmlString(this.Script);
            string str = "Scripts";
            List<IHtmlContent> item = this.ViewContext.TempData[str] as List<IHtmlContent>;
            if (item != null)
            {
                item.Add(htmlString);
            }
            else
            {
                List<IHtmlContent> list = new List<IHtmlContent>();
                list.Add(htmlString);
                item = list;
            }

            this.ViewContext.TempData[str] = item;
        }

        private TagHelperOutput WrapInputElement(TagHelperOutput inputElement)
        {
            TagBuilder tagBuilder;
            TagBuilder tagBuilder1;
            IHtmlContentBuilder htmlContentBuilder;
            IHtmlContentBuilder htmlContentBuilder1;
            IHtmlContentBuilder htmlContentBuilder2;
            TagHelperContent tagHelperContent;
            TagBuilder tagBuilder2 = new TagBuilder("span");
            TagBuilder tagBuilder3 = new TagBuilder("label");
            FormInputType inputType = this.InputType;
            if (inputType == FormInputType.File)
            {
                tagBuilder = new TagBuilder("label");
                tagBuilder.AddCssClass("custom-file-label");
                tagBuilder.Attributes.Add("for", this.Id);
                htmlContentBuilder = tagBuilder.InnerHtml.AppendHtml("Dosya Seçin");
                tagBuilder1 = new TagBuilder("div");
                htmlContentBuilder1 = tagBuilder1.InnerHtml.AppendHtml(inputElement);
                htmlContentBuilder2 = tagBuilder1.InnerHtml.AppendHtml(tagBuilder);
                inputElement = TagHelperExtension.CreateTagHelperOutput("div");
                inputElement.Attributes.SetAttribute("class", "custom-file");
                tagHelperContent = inputElement.Content.AppendHtml(tagBuilder1.InnerHtml);
                return inputElement;
            }

            if (inputType == FormInputType.Switch)
            {
                tagBuilder3.InnerHtml.AppendHtml(inputElement);
                tagBuilder3.InnerHtml.AppendHtml(tagBuilder2);
                TagBuilder tagBuilder4 = new TagBuilder("span");
                tagBuilder4.AddCssClass("switch switch-primary");
                tagBuilder4.InnerHtml.AppendHtml(tagBuilder3);
                inputElement = TagHelperExtension.CreateTagHelperOutput("div");
                inputElement.Attributes.SetAttribute("class", "col");
                inputElement.Content.AppendHtml(tagBuilder4);
                return inputElement;
            }

            if (inputType == FormInputType.Select)
            {
                var div = TagHelperExtension.CreateTagHelperOutput("div");
                div.Attributes.SetAttribute("class", "col");
                div.Content.AppendHtml(inputElement);
                return div;
            }

            if (inputType == FormInputType.Image)
            {
                tagBuilder = new TagBuilder("label");
                tagBuilder.AddCssClass("custom-file-label");
                tagBuilder.Attributes.Add("for", this.Id);
                htmlContentBuilder = tagBuilder.InnerHtml.AppendHtml("Dosya Seçin");
                tagBuilder1 = new TagBuilder("div");
                htmlContentBuilder1 = tagBuilder1.InnerHtml.AppendHtml(inputElement);
                htmlContentBuilder2 = tagBuilder1.InnerHtml.AppendHtml(tagBuilder);
                inputElement = TagHelperExtension.CreateTagHelperOutput("div");
                inputElement.Attributes.SetAttribute("class", "custom-file");
                tagHelperContent = inputElement.Content.AppendHtml(tagBuilder1.InnerHtml);
                return inputElement;
            }

            if (inputType == FormInputType.Checkbox)
            {
                tagBuilder3.AddCssClass("checkbox");
                tagBuilder3.InnerHtml.AppendHtml(inputElement);
                tagBuilder3.InnerHtml.AppendHtml(tagBuilder2);
                inputElement = TagHelperExtension.CreateTagHelperOutput("div");
                inputElement.Attributes.SetAttribute("class", "col checkbox-single");
                inputElement.Content.AppendHtml(tagBuilder3);
                return inputElement;
            }

            if (inputType == FormInputType.Price)
            {
                inputElement.SetAttribute("step", "0.01");
            }

            return inputElement;
        }

        private TagHelperOutput WrapResult(TagHelperOutput output, List<IHtmlContent> list)
        {
            output.TagName = ("div");
            output.TagMode = TagMode.StartTagAndEndTag;
            FormInputType inputType = this.InputType;
            if (inputType == FormInputType.Checkbox || inputType == FormInputType.Switch ||
                inputType == FormInputType.Select)
            {
                output.Attributes.SetAttribute("class", "form-group row");
            }
            else if (inputType == FormInputType.Hidden)
            {
                output.Attributes.SetAttribute("class", "d-none");
            }
            else
            {
                output.Attributes.SetAttribute("class", "form-group");
            }


            foreach (IHtmlContent htmlContent in list)
            {
                output.Content.AppendHtml(htmlContent);
            }

            return output;
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace TagHelpers.Form.Ajax
{
    [HtmlTargetElement("ajax-form")]
    public class AjaxFormTagHelper : FormTagHelper
    {
        public AjaxFormTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }

        [HtmlAttributeName("begin")] public string Begin { get; set; } = "ajaxFormBegin";
        [HtmlAttributeName("success")] public string Success { get; set; } = "ajaxFormSuccess";
        [HtmlAttributeName("failure")] public string Fail { get; set; } = "ajaxFormFail";
        [HtmlAttributeName("complete")] public string Complete { get; set; } = "ajaxFormComplete";

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add("data-ajax-method", this.Method);
            output.Attributes.Add("enctype", "multipart/form-data");
            await base.ProcessAsync(context, output);
            output.TagName = "form";
            output.Attributes.Add("data-ajax", "true");
            output.Attributes.Add("data-ajax-begin", Begin);
            output.Attributes.Add("data-ajax-failure", Fail);
            output.Attributes.Add("data-ajax-success", Success);
            output.Attributes.Add("data-ajax-complete", Complete);
        }
    }
}